<?php
$conn= new PDO('mysql:host=localhost;dbname=malita-mobile', 'root', 'root');
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$params = json_decode(file_get_contents('php://input'));

for ($i=0; $i < sizeof($params->head_array); $i++) {
$stmt1 = $conn->prepare("INSERT INTO head_basic_information(head_id, name, birth_date, contact_number, mobile_number, gender, civil_status, religion, blood_type) VALUES (null, :name, :birth_date, :contact_number, :mobile_number, :gender, :civil_status, :religion, :blood_type)");
    $stmt1->bindParam(':name', $name);
    $stmt1->bindParam(':birth_date', $birth_date);
    $stmt1->bindParam(':contact_number', $contact_number);
    $stmt1->bindParam(':mobile_number', $mobile_number);
    $stmt1->bindParam(':gender', $gender);
    $stmt1->bindParam(':civil_status', $civil_status);    
    $stmt1->bindParam(':religion', $religion);
    $stmt1->bindParam(':blood_type', $blood_type);

    $name = $params->head_array[$i++];
    $birth_date = $params->head_array[$i++];
    $contact_number = $params->head_array[$i++];
    $mobile_number = $params->head_array[$i++];
    $gender = $params->head_array[$i++];
    $civil_status = $params->head_array[$i++];
    $religion = $params->head_array[$i++];
    $blood_type = $params->head_array[$i++];    

$stmt1->execute();
$head_id = $conn->lastInsertId();
}
for ($i=0; $i < sizeof($params->spouse_array); $i++) {
    $stmt2 = $conn->prepare("INSERT INTO spouse_basic_information(spouse_id, name, contact_number, blood_type, birth_date, educational_attainment, religion, head_fk) VALUES (null, :name, :contact_number, :blood_type, :birth_date, :educational_attainment, :religion," . $head_id .")");
        $stmt2->bindParam(':name', $name1);
        $stmt2->bindParam(':contact_number', $contact_number);
        $stmt2->bindParam(':blood_type', $blood_type);
        $stmt2->bindParam(':birth_date', $birth_date);
        $stmt2->bindParam(':educational_attainment', $educational_attainment);   
        $stmt2->bindParam(':religion', $religion);

        $name1 = $params->spouse_array[$i];
        $contact_number = $params->spouse_array[$i++];
        $blood_type = $params->spouse_array[$i++];
        $birth_date = $params->spouse_array[$i++];
        $educational_attainment = $params->spouse_array[$i++];
        $religion = $params->spouse_array[$i++];
        $stmt2->execute();
        $spouse_id = $conn->lastInsertId();

}
for ($i=0; $i < sizeof($params->dependents_array); $i++) { 
    $stmt3 = $conn->prepare("INSERT INTO dependents_basic_information(dependents_id, name, birth_date, gender, relation, h_fk) VALUES (null, :name, :birth_date, :gender, :relation, " . $head_id .")");
        $stmt3->bindParam(':name', $name2);
        $stmt3->bindParam(':birth_date', $birth_date);
        $stmt3->bindParam(':gender', $gender);
        $stmt3->bindParam(':relation', $relation);

        $name2 = $params->dependents_array[$i];
        $birth_date = $params->dependents_array[$i++];
        $gender = $params->dependents_array[$i++];
        $relation = $params->dependents_array[$i++];
        $stmt3->execute();
        $dependents_id = $conn->lastInsertId();
}
for ($i=0; $i < sizeof($params->family_array); $i++) {
    $stmt4 = $conn->prepare("INSERT INTO family_basic_information(id, head_id, spouse_id, dependents_id, tracking_number, series_number, lgu, barangay, address, government_aid_received, number_of_female_children, number_of_male_children, tribe_name, tribe_type, map_points) VALUES (null," . $head_id . "," . $spouse_id . "," . $dependents_id .", :tracking_number, :series_number, :lgu, :barangay, :address, :government_aid_received, :number_of_female_children, :number_of_male_children, :tribe_name, :tribe_type, :map_points)");
        $stmt4->bindParam(':tracking_number', $tracking_number);
        $stmt4->bindParam(':series_number', $series_number);
        $stmt4->bindParam(':lgu', $lgu);
        $stmt4->bindParam(':barangay', $barangay);
        $stmt4->bindParam(':address', $address);
        $stmt4->bindParam(':government_aid_received', $government_aid_received);
        $stmt4->bindParam(':number_of_female_children', $number_of_female_children);
        $stmt4->bindParam(':number_of_male_children', $number_of_male_children);
        $stmt4->bindParam(':tribe_name', $tribe_name);
        $stmt4->bindParam(':tribe_type', $tribe_type);
        $stmt4->bindParam(':map_points', $map_points);

        $tracking_number = $params->family_array[$i++];
        $series_number = $params->family_array[$i++];
        $lgu = $params->family_array[$i++];
        $barangay = $params->family_array[$i++];
        $address = $params->family_array[$i++];
        $government_aid_received = $params->family_array[$i++];
        $number_of_female_children = $params->family_array[$i++];
        $number_of_male_children = $params->family_array[$i++];
        $tribe_name = $params->family_array[$i++];
        $tribe_type = $params->family_array[$i++];
        $map_points = $params->family_array[$i++];
        $stmt4->execute();
        $id = $conn->lastInsertId();
}
 for ($i=0; $i < sizeof($params->education_array); $i++) {
$stmt5 = $conn->prepare("INSERT INTO education(education_id, child_in_elementary, able_to_write_and_read, able_to_vote, able_attend_brgy_assembly, child_in_daycare, child_in_kinder, child_not_in_elementary, child_not_in_jr_hs, child_not_in_sr_hs, child_not_in_college, child_college_grad, child_college_level, child_hs_grad, child_hs_level, child_elementary_grad, child_elementary_level, family_fk ) VALUES (null, :child_in_elementary, :able_to_write_and_read, :able_to_vote, :able_attend_brgy_assembly, :child_in_daycare, :child_in_kinder, :child_not_in_elementary, :child_not_in_jr_hs, :child_not_in_sr_hs, :child_not_in_college, :child_college_grad, :child_college_level, :child_hs_grad, :child_hs_level, :child_elementary_grad, :child_elementary_level, ". $id .")");
    $stmt5->bindParam(':child_in_elementary', $child_in_elementary);
    $stmt5->bindParam(':able_to_write_and_read', $able_to_write_and_read);
    $stmt5->bindParam(':able_to_vote', $able_to_vote);
    $stmt5->bindParam(':able_attend_brgy_assembly', $able_attend_brgy_assembly);
    $stmt5->bindParam(':child_in_daycare', $child_in_daycare);
    $stmt5->bindParam(':child_in_kinder', $child_in_kinder);    
    $stmt5->bindParam(':child_not_in_elementary', $child_not_in_elementary);
    $stmt5->bindParam(':child_not_in_jr_hs', $child_not_in_jr_hs);
    $stmt5->bindParam(':child_not_in_sr_hs', $child_not_in_sr_hs);
    $stmt5->bindParam(':child_not_in_college', $child_not_in_college);
    $stmt5->bindParam(':child_college_grad', $child_college_grad);
    $stmt5->bindParam(':child_college_level', $child_college_level);
    $stmt5->bindParam(':child_hs_grad', $child_hs_grad);
    $stmt5->bindParam(':child_hs_level', $child_hs_level);
    $stmt5->bindParam(':child_elementary_grad', $child_elementary_grad);
    $stmt5->bindParam(':child_elementary_level', $child_elementary_level);

    $child_in_elementary = $params->education_array[$i++];
    $able_to_write_and_read = $params->education_array[$i++];
    $able_to_vote = $params->education_array[$i++];
    $able_attend_brgy_assembly = $params->education_array[$i++];
    $child_in_daycare = $params->education_array[$i++];
    $child_in_kinder = $params->education_array[$i++];
    $child_not_in_elementary = $params->education_array[$i++];
    $child_not_in_jr_hs = $params->education_array[$i++];
    $child_not_in_sr_hs = $params->education_array[$i++];
    $child_not_in_college = $params->education_array[$i++];
    $child_college_grad = $params->education_array[$i++];
    $child_college_level = $params->education_array[$i++];
    $child_hs_grad = $params->education_array[$i++];
    $child_hs_level = $params->education_array[$i++];
    $child_elementary_grad = $params->education_array[$i++];
    $child_elementary_level = $params->education_array[$i++];

$stmt5->execute();
$education_id = $conn->lastInsertId();
}
for ($i=0; $i < sizeof($params->profession_array); $i++) { 
    $stmt6 = $conn->prepare("INSERT INTO profession_education(profession_id, name, 
    age, type_of_skills, skills_acquired_through, remarks, profession_fk) VALUES (
    null, :name, :age, :type_of_skills, :skills_acquired_through, :remarks, " . $id
    .")");
    $stmt6->bindParam(':name', $name3);
    $stmt6->bindParam(':age', $age);
    $stmt6->bindParam(':type_of_skills', $type_of_skills);
    $stmt6->bindParam(':skills_acquired_through', $skills_acquired_through);
    $stmt6->bindParam(':remarks', $remarks);

    $name3 = $params->profession_array[$i];
    $age = $params->profession_array[$i++];
    $type_of_skills = $params->profession_array[$i++];
    $skills_acquired_through = $params->profession_array[$i++];
    $remarks = $params->profession_array[$i++];
    
$stmt6->execute();
$profession_id = $conn->lastInsertId();
}
for ($i=0; $i < sizeof($params->health_array); $i++) {
$stmt7 = $conn->prepare("INSERT INTO health_and_nutrition(health_id, newborn, no_of_newborn, newborn_below, no_of_newborn_below, has_children_immunize, age_last_immunize, has_malnourished_children, malnourished_notes, has_pregnancy_supplement, pregnancy_supplement, has_parental_services, parental_services, has_trained_delivery, delivery_notes, death, death_occurence_notes, sickness_children, other_sickness_children, sickness_adults, other_sickness_adults, philhealth_membership, health_fk ) VALUES (null, :newborn, :no_of_newborn, :newborn_below, :no_of_newborn_below, :has_children_immunize, :age_last_immunize, :has_malnourished_children, :malnourished_notes, :has_pregnancy_supplement, :pregnancy_supplement, :has_parental_services, :parental_services, :has_trained_delivery, :delivery_notes, :death, :death_occurence_notes, :sickness_children, :other_sickness_children, :sickness_adults, :other_sickness_adults, :philhealth_membership, ". $id .")");
    $stmt7->bindParam(':newborn', $newborn);
    $stmt7->bindParam(':no_of_newborn', $no_of_newborn);
    $stmt7->bindParam(':newborn_below', $newborn_below);
    $stmt7->bindParam(':no_of_newborn_below', $no_of_newborn_below);
    $stmt7->bindParam(':has_children_immunize', $has_children_immunize);
    $stmt7->bindParam(':age_last_immunize', $age_last_immunize);    
    $stmt7->bindParam(':has_malnourished_children', $has_malnourished_children);
    $stmt7->bindParam(':malnourished_notes', $malnourished_notes);
    $stmt7->bindParam(':has_pregnancy_supplement', $has_pregnancy_supplement);
    $stmt7->bindParam(':pregnancy_supplement', $pregnancy_supplement);
    $stmt7->bindParam(':has_parental_services', $has_parental_services);
    $stmt7->bindParam(':parental_services', $parental_services);
    $stmt7->bindParam(':has_trained_delivery', $has_trained_delivery);
    $stmt7->bindParam(':delivery_notes', $delivery_notes);
    $stmt7->bindParam(':death', $death);
    $stmt7->bindParam(':death_occurence_notes', $death_occurence_notes);
    $stmt7->bindParam(':sickness_children', $sickness_children);
    $stmt7->bindParam(':other_sickness_children', $other_sickness_children);
    $stmt7->bindParam(':sickness_adults', $sickness_adults);
    $stmt7->bindParam(':other_sickness_adults', $other_sickness_adults);
    $stmt7->bindParam(':philhealth_membership', $philhealth_membership);

    $newborn = $params->health_array[$i++];
    $no_of_newborn = $params->health_array[$i++];
    $newborn_below = $params->health_array[$i++];
    $no_of_newborn_below = $params->health_array[$i++];
    $has_children_immunize = $params->health_array[$i++];
    $age_last_immunize = $params->health_array[$i++];
    $has_malnourished_children = $params->health_array[$i++];
    $malnourished_notes = $params->health_array[$i++];
    $has_pregnancy_supplement = $params->health_array[$i++];
    $pregnancy_supplement = $params->health_array[$i++];
    $has_parental_services = $params->health_array[$i++];
    $parental_services = $params->health_array[$i++];
    $has_trained_delivery = $params->health_array[$i++];
    $delivery_notes = $params->health_array[$i++];
    $death = $params->health_array[$i++];
    $death_occurence_notes = $params->health_array[$i++];
    $sickness_children = $params->health_chips_array[$i++];
    $other_sickness_children = $params->health_array[$i++];
    $sickness_adults = $params->health_chips1_array[$i++];
    $other_sickness_adults = $params->health_array[$i++];
    $philhealth_membership = $params->health_array[$i++];

$stmt7->execute();
$health_id = $conn->lastInsertId();
}
for ($i=0; $i < sizeof($params->house_array); $i++) {
$stmt8 = $conn->prepare("INSERT INTO house_and_lot(house_id, ownership_status, distance_from_brgy_hall, gps_latitude, gps_longitude, home_settings, flood_frequency, earthquake_frequency, landslide_frequency, tornado_frequency, storm_surge, vehicular_accident, armed_conflict, disease_outbreak, access_road_distance, home_to_farm_distance, farm_to_road, bldg_type, drainage, toilet, garbage_disposal, roof_material, walling_material, vulnerability_and_risks, water_source, product_transportations, bldg_electricity, electricity_others, house_fk) VALUES (null, :ownership_status, :distance_from_brgy_hall, :gps_latitude, :gps_longitude, :home_settings, :flood_frequency, :earthquake_frequency, :landslide_frequency, :tornado_frequency, :storm_surge, :vehicular_accident, :armed_conflict, :disease_outbreak, :access_road_distance, :home_to_farm_distance, :farm_to_road, :bldg_type, :drainage, :toilet, :garbage_disposal, :roof_material, :walling_material, :vulnerability_and_risks, :water_source, :product_transportations, :bldg_electricity, :electricity_others,  ". $id .")");
    $stmt8->bindParam(':ownership_status', $ownership_status);
    $stmt8->bindParam(':distance_from_brgy_hall', $distance_from_brgy_hall);
    $stmt8->bindParam(':gps_latitude', $gps_latitude);
    $stmt8->bindParam(':gps_longitude', $gps_longitude);
    $stmt8->bindParam(':home_settings', $home_settings);
    $stmt8->bindParam(':flood_frequency', $flood_frequency);    
    $stmt8->bindParam(':earthquake_frequency', $earthquake_frequency);
    $stmt8->bindParam(':landslide_frequency', $landslide_frequency);
    $stmt8->bindParam(':tornado_frequency', $tornado_frequency);
    $stmt8->bindParam(':storm_surge', $storm_surge);
    $stmt8->bindParam(':vehicular_accident', $vehicular_accident);
    $stmt8->bindParam(':armed_conflict', $armed_conflict);
    $stmt8->bindParam(':disease_outbreak', $disease_outbreak);
    $stmt8->bindParam(':access_road_distance', $access_road_distance);
    $stmt8->bindParam(':home_to_farm_distance', $home_to_farm_distance);
    $stmt8->bindParam(':farm_to_road', $farm_to_road);
    $stmt8->bindParam(':bldg_type', $bldg_type);
    $stmt8->bindParam(':drainage', $drainage);
    $stmt8->bindParam(':toilet', $toilet);
    $stmt8->bindParam(':garbage_disposal', $garbage_disposal);
    $stmt8->bindParam(':roof_material', $roof_material);
    $stmt8->bindParam(':walling_material', $walling_material);
    $stmt8->bindParam(':vulnerability_and_risks', $vulnerability_and_risks);
    $stmt8->bindParam(':water_source', $water_source);
    $stmt8->bindParam(':product_transportations', $product_transportations);
    $stmt8->bindParam(':bldg_electricity', $bldg_electricity);
    $stmt8->bindParam(':electricity_others', $electricity_others);

    $ownership_status = $params->house_array[$i++];
    $distance_from_brgy_hall = $params->house_array[$i++];
    $gps_latitude = $params->house_array[$i++];
    $gps_longitude = $params->house_array[$i++];
    $home_settings = $params->house_array[$i++];
    $flood_frequency = $params->house_array[$i++];
    $earthquake_frequency = $params->house_array[$i++];
    $landslide_frequency = $params->house_array[$i++];
    $tornado_frequency = $params->house_array[$i++];
    $storm_surge = $params->house_array[$i++];
    $vehicular_accident = $params->house_array[$i++];
    $armed_conflict = $params->house_array[$i++];
    $disease_outbreak = $params->house_array[$i++];
    $access_road_distance = $params->house_array[$i++];
    $home_to_farm_distance = $params->house_array[$i++];
    $farm_to_road = $params->house_array[$i++];
    $bldg_type = $params->house_array[$i++];
    $drainage = $params->house_array[$i++];
    $toilet = $params->house_array[$i++];
    $garbage_disposal = $params->house_array[$i++];
    $roof_material = $params->house_array[$i++];
    $walling_material = $params->house_array[$i++];
    $vulnerability_and_risks = $params->house_chips_array[$i++];
    $water_source = $params->house_chips1_array[$i++];
    $product_transportations = $params->house_chips2_array[$i++];
    $bldg_electricity = $params->house_chips3_array[$i++];
    $electricity_others = $params->house_array[$i++];

$stmt8->execute();
$house_id = $conn->lastInsertId();
}
for ($i=0; $i < sizeof($params->family_income_array); $i++) {
$stmt9 = $conn->prepare("INSERT INTO family_income_income_expenditures(income_id, from_farming, from_fishing, from_government_employment, from_private_employment, from_ofw_remittances, from_pension, from_subsidies, from_self_employment, from_business, from_other_sources, other_source_of_income, gross_monthly_income, income_fk) VALUES (null, :from_farming, :from_fishing, :from_government_employment, :from_private_employment, :from_ofw_remittances, :from_pension, :from_subsidies, :from_self_employment, :from_business, :from_other_sources, :other_source_of_income, :gross_monthly_income, ". $id .")");
    $stmt9->bindParam(':from_farming', $from_farming);
    $stmt9->bindParam(':from_fishing', $from_fishing);
    $stmt9->bindParam(':from_government_employment', $from_government_employment);
    $stmt9->bindParam(':from_private_employment', $from_private_employment);
    $stmt9->bindParam(':from_ofw_remittances', $from_ofw_remittances);
    $stmt9->bindParam(':from_pension', $from_pension);    
    $stmt9->bindParam(':from_subsidies', $from_subsidies);
    $stmt9->bindParam(':from_self_employment', $from_self_employment);
    $stmt9->bindParam(':from_business', $from_business);
    $stmt9->bindParam(':from_other_sources', $from_other_sources);
    $stmt9->bindParam(':other_source_of_income', $other_source_of_income);
    $stmt9->bindParam(':gross_monthly_income', $gross_monthly_income);

    $from_farming = $params->family_income_array[$i++];
    $from_fishing = $params->family_income_array[$i++];
    $from_government_employment = $params->family_income_array[$i++];
    $from_private_employment = $params->family_income_array[$i++];
    $from_ofw_remittances = $params->family_income_array[$i++];
    $from_pension = $params->family_income_array[$i++];
    $from_subsidies = $params->family_income_array[$i++];
    $from_self_employment = $params->family_income_array[$i++];
    $from_business = $params->family_income_array[$i++];
    $from_other_sources = $params->family_income_array[$i++];
    $other_source_of_income = $params->family_income_array[$i++];
    $gross_monthly_income = $params->family_income_array[$i++];

$stmt9->execute();
$income_id = $conn->lastInsertId();
}
for ($i=0; $i < sizeof($params->family_expenditures_array); $i++) {
$stmt10 = $conn->prepare("INSERT INTO family_expenditures_income_expenditures(expenditures_id, food, education, medicine, power_bill, water_bill, philhealth, sss, pag_ibig, gsis, clothing, insurance, vehicle, taxes, debt_repayment, house_rental, house_helper, vices, others, other_expenditures, expenditures_fk) VALUES (null, :food, :education, :medicine, :power_bill, :water_bill, :philhealth, :sss, :pag_ibig, :gsis, :clothing, :insurance, :vehicle, :taxes, :debt_repayment, :house_rental, :house_helper, :vices, :others, :other_expenditures, ". $id .")");
    $stmt10->bindParam(':food', $food);
    $stmt10->bindParam(':education', $education);
    $stmt10->bindParam(':medicine', $medicine);
    $stmt10->bindParam(':power_bill', $power_bill);
    $stmt10->bindParam(':water_bill', $water_bill);
    $stmt10->bindParam(':philhealth', $philhealth);    
    $stmt10->bindParam(':sss', $sss);
    $stmt10->bindParam(':pag_ibig', $pag_ibig);
    $stmt10->bindParam(':gsis', $gsis);
    $stmt10->bindParam(':clothing', $clothing);
    $stmt10->bindParam(':insurance', $insurance);
    $stmt10->bindParam(':vehicle', $vehicle);
    $stmt10->bindParam(':taxes', $taxes);
    $stmt10->bindParam(':debt_repayment', $debt_repayment);
    $stmt10->bindParam(':house_rental', $house_rental);
    $stmt10->bindParam(':house_helper', $house_helper);
    $stmt10->bindParam(':vices', $vices);
    $stmt10->bindParam(':others', $others);
    $stmt10->bindParam(':other_expenditures', $other_expenditures);

    $food = $params->family_expenditures_array[$i++];
    $education = $params->family_expenditures_array[$i++];
    $medicine = $params->family_expenditures_array[$i++];
    $power_bill = $params->family_expenditures_array[$i++];
    $water_bill = $params->family_expenditures_array[$i++];
    $philhealth = $params->family_expenditures_array[$i++];
    $sss = $params->family_expenditures_array[$i++];
    $pag_ibig = $params->family_expenditures_array[$i++];
    $gsis = $params->family_expenditures_array[$i++];
    $clothing = $params->family_expenditures_array[$i++];
    $insurance = $params->family_expenditures_array[$i++];
    $vehicle = $params->family_expenditures_array[$i++];
    $taxes = $params->family_expenditures_array[$i++];
    $debt_repayment = $params->family_expenditures_array[$i++];
    $house_rental = $params->family_expenditures_array[$i++];
    $house_helper = $params->family_expenditures_array[$i++];
    $vices = $params->family_expenditures_array[$i++];
    $others = $params->family_expenditures_array[$i++];
    $other_expenditures = $params->family_expenditures_array[$i++];

$stmt10->execute();
$expenditures_id = $conn->lastInsertId();
}
for ($i=0; $i < sizeof($params->family_assets_array); $i++) {
$stmt11 = $conn->prepare("INSERT INTO family_asset_income_expenditures(asset_id, monthly_bank_savings, monthly_cooperative_savings, monthly_others_savings, personal_properties, asset_fk) VALUES (null, :monthly_bank_savings, :monthly_cooperative_savings, :monthly_others_savings, :personal_properties, " . $id . ")");
    $stmt11->bindParam(':monthly_bank_savings', $monthly_bank_savings);
    $stmt11->bindParam(':monthly_cooperative_savings', $monthly_cooperative_savings);
    $stmt11->bindParam(':monthly_others_savings', $monthly_others_savings);
    $stmt11->bindParam(':personal_properties', $personal_properties);

    $monthly_bank_savings = $params->family_assets_array[$i++];
    $monthly_cooperative_savings = $params->family_assets_array[$i++];
    $monthly_others_savings = $params->family_assets_array[$i++];
    $personal_properties = $params->assets_chips_array[$i++];

$stmt11->execute();
$head_id = $conn->lastInsertId();
}
for ($i=0; $i < sizeof($params->communication_access_array); $i++) {
$stmt12 = $conn->prepare("INSERT INTO communication_access(communication_id, local_newspaper, national_newspaper, other_access, access_frequency, radio_programs, cellular_networks, internet_providers, communication_fk) VALUES (null, :local_newspaper, :national_newspaper, :other_access, :access_frequency, :radio_programs, :cellular_networks, :internet_providers, " . $id . ")");
    $stmt12->bindParam(':local_newspaper', $local_newspaper);
    $stmt12->bindParam(':national_newspaper', $national_newspaper);
    $stmt12->bindParam(':other_access', $other_access);
    $stmt12->bindParam(':access_frequency', $access_frequency);
    $stmt12->bindParam(':radio_programs', $radio_programs);
    $stmt12->bindParam(':cellular_networks', $cellular_networks);
    $stmt12->bindParam(':internet_providers', $internet_providers);

    $local_newspaper = $params->communication_access_array[$i++];
    $national_newspaper = $params->communication_access_array[$i++];
    $other_access = $params->communication_access_array[$i++];
    $access_frequency = $params->communication_access_array[$i++];
    $radio_programs = $params->communication_chips_array[$i++];
    $cellular_networks = $params->communication_chips1_array[$i++];
    $internet_providers = $params->communication_chips2_array[$i++];

$stmt12->execute();
$communication_id = $conn->lastInsertId();
}
for ($i=0; $i < sizeof($params->psycho_social_array); $i++) {
$stmt13 = $conn->prepare("INSERT INTO psycho_social_care(psycho_id, violence_against_women, num_of_violence_against_women, violence_against_children, num_of_violence_against_children, conflict_with_law, victims_of_armed_conflict, practice_family_planning, family_planning_method, is_national_gov_helping, is_provincial_gov_helping, is_municipal_gov_helping, is_barangay_gov_helping, name_of_respondent, name_of_enumerator, availed_services, psycho_fk) VALUES (null, :violence_against_women, :num_of_violence_against_women, :violence_against_children, :num_of_violence_against_children, :conflict_with_law, :victims_of_armed_conflict, :practice_family_planning, :family_planning_method, :is_national_gov_helping, :is_provincial_gov_helping, :is_municipal_gov_helping, :is_barangay_gov_helping, :name_of_respondent, :name_of_enumerator, :availed_services, " . $id . ")");
    $stmt13->bindParam(':violence_against_women', $violence_against_women);
    $stmt13->bindParam(':num_of_violence_against_women', $num_of_violence_against_women);
    $stmt13->bindParam(':violence_against_children', $violence_against_children);
    $stmt13->bindParam(':num_of_violence_against_children', $num_of_violence_against_children);
    $stmt13->bindParam(':conflict_with_law', $conflict_with_law);
    $stmt13->bindParam(':victims_of_armed_conflict', $victims_of_armed_conflict);
    $stmt13->bindParam(':practice_family_planning', $practice_family_planning);
    $stmt13->bindParam(':family_planning_method', $family_planning_method);
    $stmt13->bindParam(':is_national_gov_helping', $is_national_gov_helping);
    $stmt13->bindParam(':is_provincial_gov_helping', $is_provincial_gov_helping);
    $stmt13->bindParam(':is_municipal_gov_helping', $is_municipal_gov_helping);
    $stmt13->bindParam(':is_barangay_gov_helping', $is_barangay_gov_helping);
    $stmt13->bindParam(':name_of_respondent', $name_of_respondent);
    $stmt13->bindParam(':name_of_enumerator', $name_of_enumerator);
    $stmt13->bindParam(':availed_services', $availed_services);

    $violence_against_women = $params->psycho_social_array[$i++];
    $num_of_violence_against_women = $params->psycho_social_array[$i++];
    $violence_against_children = $params->psycho_social_array[$i++];
    $num_of_violence_against_children = $params->psycho_social_array[$i++];
    $conflict_with_law = $params->psycho_social_array[$i++];
    $victims_of_armed_conflict = $params->psycho_social_array[$i++];
    $practice_family_planning = $params->psycho_social_array[$i++];
    $family_planning_method = $params->psycho_social_array[$i++];
    $is_national_gov_helping = $params->psycho_social_array[$i++];
    $is_provincial_gov_helping = $params->psycho_social_array[$i++];
    $is_municipal_gov_helping = $params->psycho_social_array[$i++];
    $is_barangay_gov_helping = $params->psycho_social_array[$i++];
    $name_of_respondent = $params->psycho_social_array[$i++];
    $name_of_enumerator = $params->psycho_social_array[$i++];
    $availed_services = $params->psycho_social_chips_array[$i++];

$stmt13->execute();
$psycho_id = $conn->lastInsertId();
}
exit;
?>